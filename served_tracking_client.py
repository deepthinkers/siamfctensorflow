'''
Send JPEG image to tensorflow_model_server loaded with GAN model.

Hint: the code has been compiled together with TensorFlow serving
and not locally. The client is called in the TensorFlow Docker container
'''

from __future__ import print_function

# Communication to TensorFlow server via gRPC
from grpc.beta import implementations
import tensorflow as tf
import json

# TensorFlow serving stuff to send messages
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2


# Command line arguments
tf.app.flags.DEFINE_string('server', '127.0.0.1:12344',
                           'PredictionService host:port')
tf.app.flags.DEFINE_string('request_json', '', 'path to image in JSON formatted request')
FLAGS = tf.app.flags.FLAGS


def main(_):
    host, port = FLAGS.server.split(':')
    channel = implementations.insecure_channel(host, int(port))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    # Send request
    with open(FLAGS.request_json, 'r') as f:
        # See prediction_service.proto for gRPC request/response details.
        data = json.load(f)
        print(data)
        request = predict_pb2.PredictRequest()

        request.model_spec.name = 'saved_model.pb'
        request.model_spec.signature_name = 'predict_tracking_bbox'
        request.inputs['examplar_input_image'].CopyFrom(
            tf.contrib.util.make_tensor_proto(data['initData'], shape=[1])
        )
        request.inputs['instance_input_image'].CopyFrom(
            tf.contrib.util.make_tensor_proto(data['currentData'], shape=[1])
        )
        request.inputs['target_bbox_feed'].CopyFrom(
            tf.contrib.util.make_tensor_proto(data['bbox'], shape=[4])
        )

    result = stub.Predict(request, 60.0)  # 60 secs timeout
    print(result)


if __name__ == '__main__':
    tf.app.run()
