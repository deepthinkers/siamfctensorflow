# Create SQS client
sqs = boto3.client('sqs')
hash_queue = sqs.get_queue_by_name(QueueName='td_hash_queue')


def images_uploaded():
    s3_loc = "s3://td-tenant-images-dev/1/incremental"

    message_body = {
        "tenantId": 1,
        "uploadedNum": 0,
        "location": s3_loc,
        "environment": "dev",
        "jobId": 1
    }

    message_attrs = {'name': {
        'DataType': 'String',
        'StringValue': 'ImagesUploadedToS3'
    }}

    response = hash_queue.send_message(
        MessageAttributes=message_attrs,
        MessageBody=json.dumps(message_body)
    )
    return response


def post_message(queue:str, object_id:str, tenant_id:str, message_type:str, status:str, body:dict):
    """Post a message to SQS"""

    # "_mav", as in "MessageAttributeValue"
    _mav = lambda name, value: { name: {'DataType': 'String', 'StringValue': value} }
    _mav_str = lambda name, value: { name: {'DataType': 'String', 'StringValue': value} }
    _mav_int = lambda name, value: { name: {'DataType': 'Number', 'StringValue': value} }

    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue)

    message_attrs = {
        **_mav("id", object_id),
        **_mav_int("tenantId", str(tenant_id)),
        **_mav("messageType", message_type),
        **_mav("header", status)
    }

    message_body = json.dumps(body)

    response = queue.send_message(MessageAttributes=message_attrs, MessageBody=message_body)
    message_id = response.get('MessageId')

    return message_id
