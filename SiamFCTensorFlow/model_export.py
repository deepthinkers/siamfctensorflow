import os
import shutil
import tensorflow as tf
import numpy as np
import utils
# from gan import GAN
from SiamFCTensorFlow.inference.inference_wrapper import InferenceWrapper
from SiamFCTensorFlow.utils.misc_utils import auto_select_gpu, load_cfgs
from tensorflow.python.framework import graph_util

# Command line arguments
tf.app.flags.DEFINE_string('checkpoint_dir', './SiamFCTensorFlow/Logs/SiamFC/track_model_checkpoints/SiamFC-3s-color-pretrained',
                           """Directory where to read training checkpoints.""")

tf.app.flags.DEFINE_string('output_dir', './SiamFCTensorFlow/Logs/SiamFC/track_model_serving_checkpoint/SiamFC-3s-color-pretrained-servable',
                           """Directory where to export the model.""")

tf.app.flags.DEFINE_integer('model_version', 1,
                            """Version number of the model.""")

tf.app.flags.DEFINE_string('final_tensor_name', 'init',
                           """I lied to my mother once""")

FLAGS = tf.app.flags.FLAGS


def save_graph_to_file(sess, graph, graph_file_name):
    output_graph_def = graph_util.convert_variables_to_constants(
        sess, graph.as_graph_def(), [FLAGS.final_tensor_name]
    )

    with tf.gfile.FastGFile(graph_file_name, 'wb') as f:
        f.write(output_graph_def.SerializeToString())

    return


def load_with_checkpoints():
    '''
    Loads saved model checkpoints and returns graph
    '''
    # create an empty graph for the session
    loaded_graph = tf.Graph()

    with tf.Session(graph=loaded_graph) as sess:
        # restore save model
        saver = tf.train.import_meta_graph(FLAGS.checkpoint_dir+'/model.ckpt-0.meta')
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.checkpoint_dir))

    return loaded_graph

def get_tensor_names():
    # create an empty graph for the session
    loaded_graph = tf.Graph()

    with tf.Session(graph=loaded_graph) as sess:
        saver = tf.train.import_meta_graph(FLAGS.checkpoint_dir+'/model.ckpt-0.meta')
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.checkpoint_dir))

        tensor_names = [tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
    return tensor_names

def preprocess_image(image_buffer):
    '''
    Preprocess JPEG encoded bytes to 3D float Tensor and rescales
    it so that pixels are in a range of [-1, 1]
    :param image_buffer: Buffer that contains JPEG image
    :return: 4D image tensor (1, width, height,channels) with pixels scaled
             to [-1, 1]. First dimension is a batch size (1 is our case)
    '''

    # Decode the string as an RGB JPEG.
    # Note that the resulting image contains an unknown height and width
    # that is set dynamically by decode_jpeg. In other words, the height
    # and width of image is unknown at compile-time.
    image = tf.image.decode_jpeg(image_buffer, channels=3)

    # After this point, all image pixels reside in [0,1)
    # until the very end, when they're rescaled to (-1, 1).  The various
    # adjust_* ops all require this range for dtype float.
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)

    # Networks accept images in batches.
    # The first dimension usually represents the batch size.
    # In our case the batch size is one.
    image = tf.expand_dims(image, 0)

    # Finally, rescale to [-1,1] instead of [0, 1)
    image = tf.subtract(image, 0.5)
    image = tf.multiply(image, 2.0)

    return image

def main_fxn():
    with tf.Graph().as_default() as g:
        with tf.Session(graph=g) as sess:
            # Inject placeholder into the graph
            feature_configs = {
                'image/encoded': tf.FixedLenFeature(
                    shape=[], dtype=tf.string),
            }

            examplar_feed = tf.parse_example(
                                tf.placeholder(
                                    # shape=[None, None, None, 3],
                                    dtype=tf.string,
                                    name='examplar_input'
                                ),
                                feature_configs
                            )

            instance_feed = tf.parse_example(
                                tf.placeholder(
                                    # shape=[None, None, None, 3],
                                    dtype=tf.string,
                                    name='instance_input'
                                ),
                                feature_configs
                            )

            target_bbox_feed = tf.placeholder(shape=[4],
                                           dtype=tf.float32,
                                           name='target_bbox_feed')

            # model_config, _, track_config = load_cfgs(FLAGS.checkpoint_dir)
            # g = tf.Graph()
            # with g.as_default():
            # model = InferenceWrapper()
            # restore_fn = model.build_graph_from_config(
            #     model_config,
            #     track_config,
            #     FLAGS.checkpoint_dir
            # )

            saver = tf.train.import_meta_graph(FLAGS.checkpoint_dir+'/model.ckpt-0.meta')
            saver.restore(sess, tf.train.latest_checkpoint(FLAGS.checkpoint_dir))
            print('Filename Tensor Name: '+saver.saver_def.filename_tensor_name)

            saver.save(sess, FLAGS.output_dir+'/test')

            # (re-)create export directory
            export_path = os.path.join(
                tf.compat.as_bytes(FLAGS.output_dir),
                tf.compat.as_bytes(str(FLAGS.model_version)))
            if os.path.exists(export_path):
                shutil.rmtree(export_path)

            output_graph_def = tf.graph_util.convert_variables_to_constants(
                sess, # The session is used to retrieve the weights
                tf.get_default_graph().as_graph_def(), # The graph_def is used to retrieve the nodes
                ['save/restore_all'] #output_node_names.split(",") # The output node names are used to select the usefull nodes
            )

            output_graph = FLAGS.output_dir+"/frozen_model.pb"
            with tf.gfile.GFile(output_graph, "wb") as f:
                print(f"Saving graph to {output_graph}")
                f.write(output_graph_def.SerializeToString())
            print("%d ops in the final graph." % len(output_graph_def.node))

            # create model builder
            builder = tf.saved_model.builder.SavedModelBuilder(export_path)

            # create tensors info
            # inputs
            predict_tensor_image_examplar_info = tf.saved_model.utils.build_tensor_info(examplar_feed['image/encoded'])
            predict_tensor_image_instance_info = tf.saved_model.utils.build_tensor_info(instance_feed['image/encoded'])
            predict_tensor_bbox_input_info = tf.saved_model.utils.build_tensor_info(target_bbox_feed)

            # outputs
            # predict_tensor_scale_xs_info = tf.saved_model.utils.build_tensor_info(model.scale_xs)
            # predict_tensor_response_output_info = tf.saved_model.utils.build_tensor_info(model.response_up)
            # 'save/restore_all',
            #  'init',
            #  'init_1']
            # def len_outputs(op):
            #     l = 0
            #     try:
            #         l = len(op.outputs)
            #     except:
            #         l = 0
            #     return l
            
            # tensor_names = [[tensor.name, len_outputs(tensor)] for tensor in tf.get_default_graph().as_graph_def().node]
            # for tensor in tf.get_default_graph().as_graph_def().node:
            #     print(tensor.name, type(tensor))

            # predict_tensor_init_info = g.get_tensor_by_name("init_1:0")
            # predict_tensor_save_all

            # build prediction signature
            prediction_signature = (
                tf.saved_model.signature_def_utils.build_signature_def(
                    inputs={
                        'examplar_input_image': predict_tensor_image_examplar_info,
                        'instance_input_image': predict_tensor_image_instance_info,
                        'target_bbox_feed': predict_tensor_bbox_input_info
                    },
                    outputs={
                        # 'into_info': predict_tensor_init_info
                        # 'scale_xs': predict_tensor_scale_xs_info,
                        # 'response_output': predict_tensor_response_output_info
                    },
                    method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME
                )
            )

            # # save the model
            legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
            builder.add_meta_graph_and_variables(
                sess, [tf.saved_model.tag_constants.SERVING],
                signature_def_map={
                    'predict_tracking_bbox': prediction_signature
                },
                legacy_init_op=legacy_init_op)

            builder.save()
            g.finalize()

    print(
        "Successfully exported Siamese model version '{}' into '{}'".format(
            FLAGS.model_version, FLAGS.output_dir
        )
    )


if __name__ == "__main__":
    main_fxn()
