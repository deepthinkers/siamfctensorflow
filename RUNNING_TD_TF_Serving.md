1) `docker run -it tbrooks/tensorflow-serving-devel /bin/bash`
2) Run TF SERVING
  --model_name
  --model_base_path
  --port

2) `/serving/bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server`


### To run TF serving docker image
Step 1
Window 1:
blackhole$ nvidia-docker run -it -p 12344:12344 rrr/tf-serving-dev-gpu  /bin/bash

Step 2
Window 2:
find docker container id, will be (e.g. here  08c6a3ab4886)
blackhole$ docker ps

Step 3
copy from some dir to version dir in /rajdata inside the container:
blackhole:/data/td-trained-models/SiamFC-3s-color-pretrained-servable$ docker cp 1 ecfdf1bc8953:/rajdata/3

Step 4
Window 1:
Docker$ bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=13371 --model_name=saved_model.pb --model_base_path=/rajdata

Step 5
Test it!
#### MUST BE IN: ~/rrr/tf_serving_example/tensorflow_serving
blackhole$ python3 ~/trey/served_tracking_client.py --server=localhost:13371 --request_json=/home/blackhole/trey/track2_urlsafe.js
