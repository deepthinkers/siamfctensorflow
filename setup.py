from distutils.core import setup

setup(
    name='SiamFCTensorFlow',
    version='0.1dev',
    packages=[
        'SiamFCTensorFlow',
        'SiamFCTensorFlow/assets',
        'SiamFCTensorFlow/metrics',
        'SiamFCTensorFlow/embeddings',
        'SiamFCTensorFlow/experiments',
        'SiamFCTensorFlow/datasets',
        'SiamFCTensorFlow/tests',
        'SiamFCTensorFlow/utils',
        'SiamFCTensorFlow/benchmarks',
        'SiamFCTensorFlow/scripts',
        'SiamFCTensorFlow/inference'
    ],
    include_package_data=True,
    license='Distributed under terms of the MIT license',
    long_description=open('README.md').read(),
)
